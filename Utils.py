import numpy as np
import pickle
import gzip


def generate_random_matrix(rows, columns, random_scope):
    return random_scope * (2 * np.random.rand(rows, columns) - 1.0)


def sigmoid(x):
    return 1 / (1 + np.power(np.e, -x))


def d_sigmoid(x):
    e_x = np.power(np.e, -x)
    return e_x / np.power(e_x + 1, 2)


def load_data():
    f = gzip.open('mnist.pkl.gz', 'rb')
    (train_input, train_output), (validation_input, validation_output), (test_input, test_output) = pickle.load(f)
    f.close()

    train_data = np.matrix(train_input).transpose(), np.matrix(vectorize(train_output, 10)).transpose()
    validation_data = np.matrix(validation_input).transpose(), np.matrix(vectorize(validation_output, 10)).transpose()
    test_data = np.matrix(test_input).transpose(), np.matrix(vectorize(test_output, 10)).transpose()

    return train_data, validation_data, test_data


def vectorize(x, length):
    output = []
    for i in range(len(x)):
        vector = np.zeros(length)
        vector[x[i]] = 1
        output.append(vector)
    return output


def devectorize(x):
    maximum = max(x)
    return list(x).index(maximum)


def get_batches(data, batch_size):
    data_input, data_output = data

    input_array = np.array(np.transpose(data_input))
    output_array = np.array(np.transpose(data_output))

    # rng_state = np.random.get_state()
    # np.random.shuffle(input_array)
    # np.random.set_state(rng_state)
    # np.random.shuffle(output_array)

    batch_count = len(output_array) / batch_size
    input_splitted = np.split(input_array, batch_count)
    output_splitted = np.split(output_array, batch_count)

    return [(np.matrix(x).transpose(), np.matrix(y).transpose()) for (x,y) in zip(input_splitted, output_splitted)]
