import numpy as np
import Network as nn
import Utils


def output_cost_function(desired_output, actual_output):
    output = []

    for i in range(len(desired_output)):
        index = actual_output[i]
        output.append(-np.log(desired_output[i][index]))

    return output


if __name__ == '__main__':
    training_data, validation_data, test_data = Utils.load_data()
    network = nn.Network(
        layers = [784, 100, 10],
        random_scope = 0.1,
        use_momentum = True,
        momentum_rate = 0.001,
        use_dynamic_learn_rate = False,
        learn_rate_decrease = 1.2,
        decrease_trigger_treshold = 0.001
    )
    network.train(
        training_data,
        validation_data,
        batch_size = 50,
        learn_rate = 0.005,
        required_successfulness = 0.95)

    print "Testing..."

    successfulness = network.validate(test_data)

    print "Testing complete. Successfulness:", successfulness
    print "END"
