import matplotlib.pyplot as plt
import numpy as np
np.seterr(divide='ignore', invalid='ignore')
import Utils


def generate_random_matrix(rows, columns, random_scope):
    return random_scope * (2 * np.random.rand(rows, columns)- 1.0)


def generate_weights(layers, random_scope):
    return [generate_random_matrix(layers[i], layers[i-1] + 1, random_scope) for i in range(1, len(layers))]


class Network:
    def __init__(self, layers, random_scope, use_momentum, momentum_rate, use_dynamic_learn_rate, learn_rate_decrease, decrease_trigger_treshold):
        self.layer_count = len(layers) - 1
        self.weights = generate_weights(layers, random_scope)
        self.use_momentum = use_momentum
        self.momentum_rate = momentum_rate
        self.use_dynamic_learn_rate = use_dynamic_learn_rate
        self.learn_rate_decrease = learn_rate_decrease
        self.decrease_trigger_treshold = decrease_trigger_treshold

    """ Feeds input forward through network """
    """ input: input_size x input_count (i.e. 784x?) """

    """ Returns: output_size x input_count (i.e. 10x?) """
    def feed_forward(self, input):
        computed_values = []
        activated_values = [np.vstack((input, np.ones((1, input.shape[1]))))]
        layer_input = input

        for i in range(self.layer_count):
            layer_input = np.vstack((layer_input, np.ones((1, layer_input.shape[1]))))

            computed = np.dot(self.weights[i], layer_input)
            computed_values.append(computed)
            layer_input = activation_function(computed)

            if i == self.layer_count - 1:
                one_hot(activation_function(computed))
            activated_values.append(layer_input)

        return computed_values, activated_values

    """ Back propagates errors through network """
    """ computed_values: output_size x input_count (i.e. 10x?) """
    """ activated_values: matches computed_values shape """
    """ train_output: output_size x input_count (i.e. 10x?) """

    """ Returns: matches network weights """
    def back_propagate(self, computed_values, activated_values, train_output):
        nabla_w = [np.zeros(w.shape) for w in self.weights]

        delta = activated_values[-1] - np.multiply(train_output, activation_function_derivative(computed_values[-1]))
        nabla_w[-1] = np.dot(delta, np.transpose(np.vstack((activated_values[-2], np.ones((1, activated_values[-1].shape[1]))))))

        for l in xrange(2, self.layer_count + 1):
            activation_derivative = activation_function_derivative(computed_values[-l])

            ones = np.ones((1, activation_derivative.shape[1]))
            vstack = np.vstack((activation_derivative, ones))
            delta = np.multiply(np.dot(np.transpose(self.weights[-l + 1]), delta), vstack)
            nabla_w[-l] = np.dot(delta, np.transpose(activated_values[-l-1]))[:delta.shape[0] - 1]

        return nabla_w

    """ Trains network until required successfulness is achieved """
    """ training_data: input_size x input_count (i.e. 784x10000) """
    """ validation_data: input_size x input_count (i.e. 784x500) """
    """ batch_size: size of single batch (i.e. 50) """
    """ learn_rate: length of one step (i.e. 0.5) """
    """ required_successfulness: successfulness that stops learning (i.e. 0.9) """
    def train(self, training_data, validation_data, batch_size, learn_rate, required_successfulness):
        successfulness = 0.0
        epoch = 1

        print "Learning..."
        while successfulness < required_successfulness:
            batches = Utils.get_batches(training_data, batch_size)

            previous_weights = np.copy(self.weights)
            previous_successfulness = successfulness

            for batch in batches:
                train_input, train_output = batch

                computed_values, activated_values = self.feed_forward(train_input)
                nabla_w = self.back_propagate(computed_values, activated_values, train_output)
                self.weights = [w - learn_rate * nw / batch_size for w, nw in zip(self.weights, nabla_w)]

                if self.use_momentum:
                    for i in range(self.layer_count):
                        diff = self.weights[i] - previous_weights[i]
                        self.weights[i] += self.momentum_rate * diff

            successfulness = self.validate(validation_data)

            if self.use_dynamic_learn_rate:
                diff = successfulness - previous_successfulness
                if diff < self.decrease_trigger_treshold:
                    learn_rate *= self.learn_rate_decrease
                    print "Decreasing learn rate to", learn_rate
                    #plt.imsave('weights.png', self.weights[0], cmap="gray")
                    #successfulness = 1.0

            print "Epoch", epoch, "end."
            print "Successfulness:", successfulness
            print ""
            epoch += 1

        print "Training succeeded."

    def validate(self, validation_data):
        validation_input, validation_output = validation_data

        network_feed = self.feed_forward(validation_input)
        network_results = np.transpose(network_feed[1][-1])

        correct = 0.0
        all = validation_output.shape[1]
        for i in range(all):
            devectorized_output = np.argmax(network_results[i])
            devectorized_desired = np.argmax(validation_output[:, i])
            if devectorized_output == devectorized_desired:
                correct += 1.0

        return correct / all










# Sigmoid
def activation_function(x):
    return np.log(1 + np.power(np.e, x))
    #return 1 / (1 + np.power(np.e, -x))


# Sigmoid derivative
def activation_function_derivative(x):
    e_x = np.power(np.e, -x)
    # return e_x / np.power(e_x + 1, 2)
    return e_x / (e_x + 1)


# One-hot
def one_hot(x):
    e_values = np.power(np.e, x)
    sum = np.sum(e_values, axis = 0)
    if (sum == np.nan).any():
        print "Boo!"
    return e_values / sum


def output_cost_function(desired_output, actual_output):
    return -np.log(actual_output[desired_output])


def devectorize(x):
    return np.argmax(x)


def vectorize(x, length):
    output = []
    for i in range(len(x)):
        vector = np.zeros(length)
        vector[x[i]] = 1
        output.append(vector)
    return output


class OldNetwork:
    def __init__(self, layers, use_momentum = True, use_dynamic_learn_rate = True, random_scope = 0.1):
        self.layer_count = len(layers) - 1
        weights, biases = generate_weights_and_biases(layers, random_scope)
        self.weights = weights
        self.biases = biases

        self.use_momentum = use_momentum
        self.use_dynamic_learn_rate = use_dynamic_learn_rate

    def feed_forward(self, input):
        computed_values = []
        activated_values = []

        layer_input = np.transpose(input)

        for i in range(self.layer_count):
            computed = np.dot(self.weights[i], layer_input)# + np.sum(self.biases[i], axis=1)
            computed_values.append(computed)
            layer_input = activation_function(computed)

            if i == self.layer_count - 1:
                layer_input = one_hot(layer_input)

            activated_values.append(layer_input)

        return computed_values, activated_values

    def validate(self, validation_data):
        validation_input, validation_output = validation_data

        network_feed = self.feed_forward(validation_input)
        network_results = np.transpose(network_feed[1][1])

        correct = 0.0
        all = len(validation_output)

        for i in range(all):
            devectorized = devectorize(network_results[i])
            if devectorized == validation_output[i]:
                correct += 1.0

        return correct / all

    def back_propagate(self, computed_values, activated_values, train_output):
        nabla_b = [np.zeros(b.shape) for b in self.biases]
        nabla_w = [np.zeros(w.shape) for w in self.weights]

        delta = activated_values[-1] - np.transpose(vectorize(train_output, 10)) * activation_function_derivative(computed_values[-1])
        nabla_b[-1] = delta
        nabla_w[-1] = np.dot(delta, np.transpose(activated_values[-2]))

        for l in xrange(2, self.layer_count):
            activation_derivative = activation_function_derivative(computed_values[-l])
            delta = np.dot(np.transpose(self.weights[-l + 1]), delta) * activation_derivative
            nabla_b[-l] = delta
            nabla_w[-l] = np.dot(delta, np.transpose(activated_values[-l - 1]))

        return nabla_w, nabla_b

